var express = require('express');
var healthcheck = require('healthcheck-middleware');
var request = require('request');
var os = require('os');

var http_port = process.env.PORT || 5000;
var app = express();


app.use('/healthcheck', healthcheck());

app.get('/info', function(req, res) {
	res.send({
		message: "here is a very interesting message",
		environment: process.env.NODE_ENV,
		host: os.hostname(),
		os: os.platform()
	});
});

app.get('/vod/diagnostic', function(req, res) {
    req.pipe(request.get('https://vcm-origin.nscreen.iptv.bell.ca/api/vod/v3/diagnostic')).pipe(res);
});

app.listen(http_port, function () {
  console.log('Started on port', http_port);
});