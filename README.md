# Docker Swarm Demo #

This repository contains the backing materials for the Docker Swarm Demo

### What it contains ###

* /app : sample application used for the demo. The application includes a Dockerfile for building the release image.
* /swarm : Docker swarm demo walkthrough
