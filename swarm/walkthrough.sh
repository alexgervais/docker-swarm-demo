

## Install docker

curl -sSL https://get.docker.com/ | sh

## Initialize swarm

docker swarm init --advertise-addr eth1 --listen-addr eth1
docker swarm join ## use the output of previous command

# Validate the state of the swarm
docker node ls
docker node inspect docker-demo1 --pretty

## Pull the image
# locally 
docker build -t web:1.0.0 .
docker save web:1.0.0 > web-image
scp web-image root@{ip1}: 
scp web-image root@{ip2}: 
# on nodes
docker load < web-image
docker load < web-image

# Create the service
docker network create -d overlay app-network
docker service create --name web --publish 80:5000 --network app-network --env NODE_ENV=production web:1.0.0

docker service ls # note the service is now shown
docker service ps web # see the details of the service 

## try application
# on node
curl localhost/info #notice load balancing
# locally
curl {ip1}/info #notice load balancing


# Simulate drain
docker node update --availability drain docker-demo2
docker service ps web

